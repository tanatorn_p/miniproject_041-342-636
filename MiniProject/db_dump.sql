-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for preorder_system
CREATE DATABASE IF NOT EXISTS `preorder_system` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `preorder_system`;

-- Dumping structure for table preorder_system.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `Name` varchar(100) NOT NULL,
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `Address` varchar(100) NOT NULL,
  `City` varchar(100) NOT NULL,
  `Province` varchar(100) NOT NULL,
  `Postal Code` int(11) DEFAULT '0',
  `Country` varchar(100) NOT NULL,
  `EmailAddress` varchar(100) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Role` int(11) NOT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table preorder_system.customers: ~3 rows (approximately)
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
REPLACE INTO `customers` (`Name`, `UserID`, `Address`, `City`, `Province`, `Postal Code`, `Country`, `EmailAddress`, `Username`, `Password`, `Role`) VALUES
	('Tanatorn Pethmunee', 1, '88 Duangchan Rd.', 'Hadyai', 'Songkhla', 90110, 'Thailand', 'tanatorn.p@gmail.com', 'tanatorn.p', '$2y$10$BcnD0htkZSfXsrKjbgIxv.fANaBtbjEdPBqFl23fiSqW2UIJPp4XC', 0),
	('a', 2, 'b', 'c', 'd', 5, 'e', 'x@mail.com', 'admin', '$2y$10$YJ0B0x65i6knx02JrnWFm.PADJQtKqHLKJPzb4ou0e1jmVESr3Zfy', 1),
	('example', 3, 'example', 'example', 'example', 1, 'example', 'example@mail.com', 'example', '$2y$10$YdgQFN5qaD0g/pXgrQOZxurucyZoMDROrUnA2c5Smw4AsSNsnsgu2', 0);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table preorder_system.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `OrderID` int(11) NOT NULL AUTO_INCREMENT,
  `ProductID` int(11) DEFAULT '0',
  `Quantity` int(11) DEFAULT '0',
  `ConfirmOrder` char(50) DEFAULT 'n',
  `UserID` int(11) DEFAULT '0',
  PRIMARY KEY (`OrderID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table preorder_system.orders: ~4 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
REPLACE INTO `orders` (`OrderID`, `ProductID`, `Quantity`, `ConfirmOrder`, `UserID`) VALUES
	(1, 1, 1, 'n', 1),
	(2, 1, 3, 'n', 1),
	(3, 1, 2, 'n', 3),
	(4, 3, 1, 'y', 3);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table preorder_system.products
CREATE TABLE IF NOT EXISTS `products` (
  `Name` varchar(50) NOT NULL,
  `ProductID` int(11) NOT NULL AUTO_INCREMENT,
  `Price` int(11) NOT NULL,
  `QuantityInStock` int(11) NOT NULL,
  `QuantityInOrder` int(11) NOT NULL,
  PRIMARY KEY (`ProductID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table preorder_system.products: ~2 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
REPLACE INTO `products` (`Name`, `ProductID`, `Price`, `QuantityInStock`, `QuantityInOrder`) VALUES
	('clothes', 1, 100, 20, 5),
	('sunglasses', 3, 250, 8, 1);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
