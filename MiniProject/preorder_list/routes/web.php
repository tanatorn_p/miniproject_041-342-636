<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Firebase\JWT\JWT;

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/hello', function () {
              return 'Hello World';
});
$router->get('/list_product', ['middleware' => 'auth', function () {
	$results = app('db')->select("SELECT * FROM products");
    return response()->json($results);
}]);
$router->post('/login', function(Illuminate\Http\Request $request) {

    $username = $request->input("username");
    $password = $request->input("password");

    $result = app('db')->select("SELECT UserID, password, role FROM customers WHERE Username=?",
                                    [$username]);

    $loginResult = new stdClass();

    if(count ($result) == 0) {
            $loginResult->status = "fail";
            $loginResult->reason = "User is not founded";
    }else {

        if(app('hash')->check($password, $result[0]->password)){
            $loginResult->status = "success";

            $payload = [
                'iss' => "preorder_system",
                'sub' => $result[0]->UserID,
                'iat' => time(),
                'exp' => time()+ 30 * 60 * 60,

            ];

            $loginResult->token = JWT::encode($payload, env('APP_KEY'));
            $loginResult->role = $result[0]->role;

        }else {
            $loginResult->status = "fail";
            $loginResult->reason = "Incorrect Password";
        }
    }
    return response()->json($loginResult);
});
$router->post('/register', function(Illuminate\Http\Request $request) {

    $name = $request->input("name");
    $address = $request->input("address");
	$Province = $request->input("province");
	$City	= $request->input("city");
	$Country = $request->input("country");
	$Postal_Code = $request->input("postal_code");
    $emailaddress = $request->input("emailaddress");
    $username = $request->input("username");
    $password = app('hash')->make($request->input("password"));

    $query = app('db')->insert('INSERT into customers
                    (Username, Password, Name, Address, Province,City,Country,Postal_Code,Emailaddress, role)
                    VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
                    [ $username,
                      $password,
                      $name,
                      $Address,
					  $Province,
					  $City,
					  $Country,
					  $Postal_Code,					
                      $emailaddress,
                      'n'] );
    return "Ok";

});

$router->post('/add_product', function(Illuminate\Http\Request $request) {
	$product_name = $request->input("product_name");
	$product_price = $request->input("product_price");
	$product_stock = $request->input("product_stock");
	$product_order = $request->input("product_order");

	$query = app('db')->insert('INSERT into products
									(Name, Price, QuantityInStock, QuantityInOrder)
									VALUES (?, ?, ?, ?)',
									[ $product_name,
									  $product_price,
									  $product_stock,
									  $product_order] );
	return "Ok";

});

$router->put('/update_product', function(Illuminate\Http\Request $request) {
	$product_id = $request->input("product_id");
	$product_name = $request->input("product_name");
	$product_price = $request->input("product_price");
	$product_stock = $request->input("product_stock");
	$product_order = $request->input("product_order");

	$query = app('db')->update('UPDATE products
									SET Name=?,
										Price=?, 
										QuantityInStock=?,
										QuantityInOrder=?
									WHERE
										ProductID=?',									
									 [ $product_name,
									   $product_price,
									   $product_stock,
									   $product_order,
									   $product_id] );
	return "Ok";

});

$router->delete('/delete_product', function(Illuminate\Http\Request $request) {
	$product_id = $request->input("product_id");

	$query = app('db')->delete('DELETE FROM products									
									WHERE
										ProductID=?',									
									 [ 
									   $product_id] );
	return "Ok";

});

$router->post('/customer', function(Illuminate\Http\Request $request) {
	
	$name = $request->input("name");
	$address = $request->input("address");
	$city = $request->input("city");
	$province = $request->input("province");
	$postal = $request->input("postal");
	$country = $request->input("country");
	$email = $request->input("email");
	$username = $request->input("username");
	$password = app('hash')->make($request->input("password"));

	$query = app('db')->insert('INSERT into customers
									(Name, Address, City, Province, PostalCode, Country, EmailAddress, Username, Password, Role)
									VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
									[ $name,
									  $address,
									  $city,
									  $province,
									  $postal,
									  $country,
									  $email,
									  $username,
									  $password,
									  0, ] );
	return "Ok";

});

$router->post('/orders', ['middleware' => 'auth', function(Illuminate\Http\Request $request) {
	
	$product_id = $request->input("product_id");
	$product_q = $request->input("product_q");

	$user = app('auth')->user();
	$userID = $user->id;

	$query = app('db')->insert('INSERT into orders
									(ProductID, Quantity, ConfirmOrder, UserID)
									VALUES (?, ?, ?, ?)',								
									 [ $product_id,
									   $product_q,
									   'n',
									   $userID
									    ] );
	return "Ok";

}]);

$router->get('/list_order', function() {
	$results = app('db')->select("SELECT OrderID,
										 orders.ProductID,
										 products.Price,
										 Quantity,										 
										 ConfirmOrder
									FROM products, orders, customers
									WHERE (orders.ProductID = products.ProductID) AND
									       (customers.UserID = orders.UserID)
										    ");
	return response()->json($results);
});

$router->get('/list_order/{product_id}', function($product_id) {
	$results = app('db')->select("SELECT OrderID,
										 customers.Name,										 
										 customers.Address,
										 customers.City,
										 customers.Province,
										 customers.PostalCode,
										 customers.Country,
										 customers.EmailAddress,
										 orders.ProductID,
										 products.Price,
										 Quantity,										 
										 ConfirmOrder
									FROM products, orders, customers
									WHERE (orders.ProductID = products.ProductID)
									       AND (products.ProductID = ?)
										   AND (customers.UserID = orders.UserID)",
										   [$product_id]);
	return response()->json($results);
});

$router->put('/confirm_order', ['middleware' => 'auth', function(Illuminate\Http\Request $request) {
	
	$order_id = $request->input("order_id");
	
	$user = app('auth')->user();
	$userID = $user->id;
	
	$result = app('db')->select("SELECT role FROM customers WHERE UserID = ?", [$userID]);
	if($result[0]->role == 1) {
		
		$query = app('db')->insert('UPDATE orders
									SET confirmorder="y"
									WHERE
										OrderID=?',
									[ $order_id ]);
		return "Ok";
	} else {
		return "Unauthorized, only admin is allowed";
	}
}]);

$router->post('/login', function(Illuminate\Http\Request $request) {
	
	$username = $request->input("username");
	$password = $request->input("password");
	
	$result = app('db')->select('SELECT UserID, password from customers WHERE Username=?',
							[$username]);
							
	$loginResult = new stdClass();
	 
	if (count($result) == 0) {
		$loginResult->status = "fail";
		$loginResult->reason = "User is not founded";
	} else {
		if (app('hash')->check($password, $result[0]->password)) {
			$loginResult->status = "success";
			
			$payload = [
				'iss' => "preorder_system", 
				'sub' => $result[0]->UserID,
				'ist' => time(),
				'exp' => time() + 30 * 60 * 60,
			];
			
			$loginResult->token = JWT::encode($payload, env('APP_KEY'));
			
			return response()->json($loginResult);
		} else {
			$loginResult->status = "fail";
			$loginResult->reason = "Incorrect Password";
			return response()->json($loginResult);
		}
	}
	
	return response()->json($loginResult);

});

