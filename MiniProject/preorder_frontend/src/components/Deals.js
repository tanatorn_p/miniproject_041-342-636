import React from "react";
import "./Deals.css";
import Item from "./Item";

function Deals() {
  return (
    <div className="cards">
      <h1>Check out these Deals!</h1>
      <div className="container">
        <div className="wrapper">
          <ul className="items">
            <Item
              src="nobleshirt.jpg"
              text="Noble Cloth"
              label="Cloths"
              path="/products"
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Deals;
