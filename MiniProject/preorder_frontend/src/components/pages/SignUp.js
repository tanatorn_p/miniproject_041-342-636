import { useState } from 'react';
import axios from 'axios';
import './Signup.css'
import {Link} from 'react-router-dom';
import {
	Redirect
} from "react-router-dom";

function SignUp() {

	const [name, 	setName] 	= useState("");
	const [address,	setAddress]	= useState("");
	const [city,	setCity]	= useState("");
	const [province,	setProvince]	=useState("");
	const [postal,	setPostal]	= useState("");
	const [country,	setCountry]	=	useState("");
	const [email,  	setEmail] 	= useState("");
	const [username,setUsername]= useState("");
	const [pw1,  	setPw1] 	= useState("");
	const [pw2,  	setPw2] 	= useState("");
	const [isSuccess, setIsSuccess] = useState(false);
	function sendSignup(){
			
		if (pw1 != pw2) {			
			alert ("Password ไม่ตรงกันครับ กรุณากรอกใหม่");
		}else{
		
			axios.post('http://localhost/api/v1/register',
				{
					"name" : name,
					"address" : address,
					"emailaddress" : email,
					"username" : username,
					"password" : pw1,
					"city" : city,
					"province" : province,
					"postal"	: postal,
					"country"	: country,
					
				}
			).then (
				res=> {	
					
					if (res.data == "Ok") {
							alert("Regis success");
							setIsSuccess(true)
					}else {
							alert ("We found a problem can't regis");
							setIsSuccess(false)
					}
				}
			);
				
		}
		
	}
	
	
	return (<div className="Register">
			<br/>
			<h1> Sign Up </h1>
			<br/>
			
			<div className="regisinfo">
			<h2> Name: <input type="text" value={name} 					onChange={(e)=>{setName(e.target.value)}}	/ > </h2>
			<h2> Address : <input type="text" value={address} 			onChange={(e)=>{setAddress(e.target.value)}}	/ > </h2>
			<h2> City : <input type="text" value={city} 			 	onChange={(e)=>{setCity(e.target.value)}}	/ > </h2>
			<h2> Province : <input type="text" value={province} 		onChange={(e)=>{setProvince(e.target.value)}}	/ > </h2>
			<h2> Country : <input type="text" value={country} 			onChange={(e)=>{setCountry(e.target.value)}}	/ > </h2>
			<h2> Postal Code  : <input type="text" value={postal} 		onChange={(e)=>{setPostal(e.target.value)}}	/ > </h2>
			<h2> Email:  <input type="text" value={email}				onChange={(e)=>{setEmail(e.target.value)}}	/> </h2>
			<h2> Username: <input type="text"  value={username}			onChange={(e)=>{setUsername(e.target.value)}}	/ > </h2>
			<h2> Password: <input type="password"  value={pw1}			onChange={(e)=>{setPw1(e.target.value)}}	/ > </h2>
			<h2> Confirm Password: <input type="password" value={pw2}	onChange={(e)=>{setPw2(e.target.value)}}  / > </h2>
			<br/><br/>
			
			<button className="button" onClick={()=>sendSignup()}>Register</button>
			<h4><Link to="/login">Login</Link></h4>
			</div>
			{isSuccess && <Redirect to="/" /> }
	</div>);

}

export default SignUp;