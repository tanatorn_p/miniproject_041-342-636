import {
	Link,
	Redirect
  } from "react-router-dom";
  
  
  import { useState, useEffect } from 'react';
  import axios from 'axios';
  import './Login.css';

  function Login() {
	  
	  const [username, setUsername] = useState("");
	  const [password, setPassword] = useState("");
	  const [isLoggedInToMember, setIsLoggedInToMember] = useState(false);
	  const [isLoggedInToAdmin,  setIsLoggedInToAdmin] =  useState(false);
	  
	  
	  useEffect (()=>{
		  //UseEffect จะทำงานเมื่อ Component ถูกโหลดมาตอนครั้งแรก
		  //เราจะใช้ Check ว่า User Login ไว้หรือยัง  ถ้ายัง ให้ Login แต่ถ้า Login แล้ว ให้วิ่งไปหน้า Member Area เลย
		  //alert(sessionStorage.getItem('api_token')==null);
		  
		  if (sessionStorage.getItem('user_api_token')!=null) {
			  setIsLoggedInToMember(false);
		  }
		  
		  if (sessionStorage.getItem('admin_api_token')!=null) {
			  setIsLoggedInToAdmin(false);
		  }
	  
	  }, []);
	  
	  function sendLogin(){	
	  
			  axios.post('http://localhost/api/v1/Login',
				  {
					  "username" : username,
					  "password" : password,
				  }
			  ).then (
				  res=> {				
					  if (res.data.status=="success"){
						  //SessionStorage เป็นคำสั่งมาตารฐานของ Javascript สำหรับการเก็บข้อมูลใดๆที่ใช้ชั่วคราว กรณีนี้เราใช้ Session Storage เก็บ api_token					
						  if ( res.data.role == 'n'){
							  setIsLoggedInToMember(true);
							  sessionStorage.setItem('user_api_token', res.data.token);
						  }else{
							  setIsLoggedInToAdmin(true);
							  sessionStorage.setItem('admin_api_token', res.data.token);
						  }
					  }else {
						  alert ("ล็อคอินไม่สำเร็จ");
					  }
				  }
			  );	
	  }
	  
	  return (
		  <div>
			  <br/>
			  <h1> LOGIN</h1>
			  <br/>

			  <div className="logininfo">
			  <h2> Username: <input type="text" onChange={(e)=>{setUsername(e.target.value)}}/ > </h2>
			  <h2> Password: <input type="password" onChange={(e)=>{setPassword(e.target.value)}}/ > </h2>
			  <br/>
			  <button className="button" onClick={()=>{sendLogin()}} >Submit</button>
			  <br/><br/><br/>
			  <h4><Link to="/sign-up">Register</Link></h4>
			  {isLoggedInToMember && <Redirect to="/Home" /> }
			  {isLoggedInToAdmin &&  <Redirect to="/Home" /> }
			  </div>
		  </div>
	  
	  );
  }
  
  export default Login;