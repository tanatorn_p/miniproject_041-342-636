import React from 'react';
import '../../App.css';
import Deals from '../Deals';
import HeroSection from '../Main';

function Home() {
  return (
    <>
      <HeroSection />
      <Deals />
    </>
  );
}

export default Home;
