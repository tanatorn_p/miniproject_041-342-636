import React from "react";
import "../App.css";
import { Button } from "./Button";
import "./Main.css";
import { Link } from "react-router-dom";

function Main() {
  return (
    <div className="hero-container">
      <video src="/videos/video-1.mp4" autoPlay loop muted />
      <h1>Orders are waiting for you </h1>
      <p>What are you waiting for?</p>
      <div className="hero-btns">
        <Link
          to="/Login" className="link"
        >
          ALREADY HAVE ACCOUNT?
        </Link>
        <Button
          className="btns"
          buttonStyle="btn--primary"
          buttonSize="btn--large"
        >
          NEW CUSTOMER <i className="far fa-play-circle" />
        </Button>
      </div>
    </div>
  );
}

export default Main;
